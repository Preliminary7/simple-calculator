// Name: Xao Thao
// Date: 09/06/2019
// Project Name: Simple Calculator
// Project Descriptions: Make a calculator that will do the following addition, subtraction, multiplication, divide 
// And extra credit for POW.


#include <iostream>
#include <conio.h>

//--------Global Variables---------

using namespace std;
char b; // declare global character

float num1, num2, num3, num4, num5;
float a;
int c;
float add(float a, float c);
float subtract(float a, float c);
float multiply(float a, float c);
bool divide(float a, float c);
int pow(int, int);




//----End of Global Variables-------


//----Beginning of function------
int main()
{
	
	
	char again = 'Y';
	float answer = 0;


	while (again == 'y' || again == 'Y')
	{
		cout << "Enter an equation:\n";
		cin >> a >> b >> c;
		if (b == '+')
		{
			num1 = a + c;
			cout << num1;
		}
		if (b == '-')
		{
			num2 = a - c;
			cout << num2;
		}
		if (b == '/')
		{
			
			if (c == '0')
			{
				cout << "Invalid number";
			}
			else
			{
				num3 = a / c;
				cout << num3;
			}
			
		}
		
		if (b == '*')
		{
			num4 = a * c;
			cout << num4;
		}
		if (b == '^')
		{
			int base, powerRaised, result;
			cout << "Enter base number: ";
			cin >> base;
			cout << "Enter power number(positive integer): ";
			cin >> powerRaised;
			result = pow(base, powerRaised);
			cout << base << "^" << powerRaised << " = " << result;

			
		}
		
		cout << "\n";
		cout << "Go again?(Y/N)";
		cin >> again;
	}
	//end while loop


	
	return 0;
}

//----End of function-------

//----beginning of class----


float add(float a, float c)
{
	return num1;
}

float subtract(float a, float c)
{
	return num2;
}

bool divide(float a, float c)
{
	return num3;
}

float multiply(float a, float c)
{
	return num4;
}

int pow(int base, int powerRaised)
{
	if (powerRaised != 0)
		return (base * pow(base, powerRaised - 1));
	else
		return 1;
}
